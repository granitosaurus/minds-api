=====
Usage
=====

Without Authentication
----------------------
To use minds-api in a project without authenticating::

    form minds import Minds

    # create Minds api object
    minds_api = Minds()


For example to retrieve top newsfeed::

    results = minds_api.newsfeed_top()
    # {"status": "success", "activity": [], ...}

If for some reason api requests fails it will return::

    {"status": "error", "message": "some error message"}


With Authentication
----------------------

To authenticate you need to pass ``Profile`` configuration to ``Minds`` object when initiating::

    from minds import Minds, Profile

    minds = Minds(Profile('username','password'))

Profile will save profile data in ``$XDG_CONFIG_HOME`` directory (usually ``~/.config``) ``minds`` subdirectory under ``username`` filename if authentication is successful.

i.e. username ``foobar`` will be stored in ``~/.config/minds/foobar``

.. note:: user credentials will be stored in plain text file in config directory

Proxies
^^^^^^^

There's also proxy support. To use a profile via proxy a valid proxy string must be passed to ``proxy`` keyword argument::

    minds = Minds(Profile('username', 'password', proxy='http://username:password@mysuperproxy.com:80')

.. note:: proxy credentials will be stored in plain text file in config directory

Cookies
^^^^^^^

If authenticated ``Minds`` api will try to optimize requests by storing cookies in user profile file. This reduces requests amount needed for pretty much every action.

.. note:: If there's trouble authenticating cookie corruption might occur, deleting profile will fix the issue



